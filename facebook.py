import os
import requests


class User(object):
    """
    A user class.
    """

    def __init__(self, email_address, first_name, last_name, user_id):
        self.email_address = email_address
        self.user_name = self.email_address.split("@")[0]
        self.first_name = first_name
        self.last_name = last_name
        self.user_id = user_id


class Messenger(object):
    def __init__(self, app_token=None):
        self.app_token = app_token or os.getenv("FACEBOOK_TOKEN")

    def send_action(self, recipient, action):
        try:
            send_url = f"https://graph.facebook.com/v2.6/me/messages?access_token={self.app_token}"
            headers = {"Authorization": "Bearer {}".format(self.app_token)}
            payload = {"recipient": recipient, "sender_action": action}
            api_response = requests.post(send_url, json=payload, headers=headers)
            if api_response.status_code != 200:
                print(api_response.text)
                raise Exception("Failed to send message")
        except Exception as e:
            print(e)
            raise Exception("Failed to send action")

    def send_message(self, message, recipient={"id": "2721587091214019"}):
        try:
            send_url = (
                "https://graph.facebook.com/v2.6/me/messages?access_token={}".format(
                    self.app_token
                )
            )
            headers = {"Authorization": "Bearer {}".format(self.app_token)}
            payload = {
                "recipient": recipient,
                "message": {"text": message},
                "messaging_type": "MESSAGE_TAG",
                "tag": "ACCOUNT_UPDATE",
            }
            api_response = requests.post(send_url, json=payload, headers=headers)
            if api_response.status_code != 200:
                print(api_response.text)
                raise Exception("Failed to send message")

        except Exception as e:
            print(e)
            raise Exception("Failed to send message")

    def get_user(self, user_id):
        try:
            print("user id: {}".format(user_id))
            member_url = "https://graph.facebook.com/v2.10/{}?fields=email,first_name,last_name".format(
                user_id
            )
            headers = {"Authorization": "Bearer {}".format(self.app_token)}
            api_response = requests.get(member_url, headers=headers)
            print(api_response)
            json_response = api_response.json()
            email_address = json_response["email"]
            first_name = json_response["first_name"]
            last_name = json_response["last_name"]
            user = User(email_address, first_name, last_name, user_id)
            return user
        except Exception as e:
            print(e)
            raise Exception("Failed to retrieve user information")

    def get_user_from_email(self, email):
        try:
            member_url = "https://graph.facebook.com/v2.10/{}?fields=id,first_name,last_name".format(
                email
            )
            headers = {"Authorization": "Bearer {}".format(self.app_token)}
            api_response = requests.get(member_url, headers=headers)
            print(api_response)
            json_response = api_response.json()
            first_name = json_response["first_name"]
            last_name = json_response["last_name"]
            user_id = json_response["id"]
            user = User(email, first_name, last_name, user_id)
            return user
        except Exception as e:
            print(e)
            raise Exception("Failed to retrieve user information")

    def get_user_id(self, email):
        try:
            member_url = "https://graph.facebook.com/v2.10/{}?fields=id,first_name,last_name".format(
                email
            )
            headers = {"Authorization": "Bearer {}".format(self.app_token)}
            api_response = requests.get(member_url, headers=headers)

            print(api_response)
            print(api_response.text)
            print(api_response.json())
            json_response = api_response.json()
            user_id = json_response.get("id")
            print(user_id)
            return user_id
        except Exception as e:
            print(e)
            raise Exception("Failed to retrieve user information")

    def send_quick_reply(self, user_id, message, quick_replies):
        try:
            send_url = (
                "https://graph.facebook.com/v2.6/me/messages?access_token={}".format(
                    self.app_token
                )
            )
            headers = {"Authorization": "Bearer {}".format(self.app_token)}
            payload = {
                "recipient": {"id": user_id},
                "message": {"text": message, "quick_replies": quick_replies},
            }
            api_response = requests.post(send_url, json=payload, headers=headers)
            if api_response.status_code != 200:
                print(api_response.text)
                raise Exception("Failed to send message")
        except Exception as e:
            print(e)
            raise Exception("Failed to send message")

    def send_url_button(self, text, title, url, recipient={"id": "2721587091214019"}):
        try:
            send_url = (
                "https://graph.facebook.com/v2.6/me/messages?access_token={}".format(
                    self.app_token
                )
            )
            headers = {"Authorization": "Bearer {}".format(self.app_token)}
            payload = {
                "recipient": recipient,
                "message": {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "button",
                            "text": text,
                            "buttons": [
                                {"type": "web_url", "url": url, "title": title}
                            ],
                        },
                    }
                },
            }
            api_response = requests.post(send_url, json=payload, headers=headers)
            if api_response.status_code != 200:
                print(api_response.text)
                raise Exception("Failed to send message")
        except Exception as e:
            print(e)
            raise Exception("Failed to send message")


if __name__ == "__main__":
    messenger = Messenger()
    messenger.send_message("Hello again from Python.")
