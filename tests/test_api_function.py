import unittest
from unittest.mock import patch
import context
from bld.api_function import APIFunction


class TestAPIFunction(APIFunction):
    endpoint = "/test"

    def get(self):
        return "test"


class APIFunctionTests(unittest.TestCase):
    @classmethod
    def setUp(self):
        self.test_api_function = TestAPIFunction()
        self.get_event = {"httpMethod": "get"}

    @patch.object(TestAPIFunction, "get")
    def test_run(self, mock_get):
        mock_get.return_value = "test"

        response = self.test_api_function.run(self.get_event, {})

        mock_get.assert_called_once_with(self.get_event)
        self.assertEqual(response, "test")

    @patch.object(TestAPIFunction, "get")
    def test_url_params(self, mock_get):
        self.test_api_function.endpoint = "/test/{test_id}/task/{task_id}"
        self.get_event["pathParameters"] = {"test_id": 1908, "task_id": 2016}
        self.test_api_function.run(self.get_event, {})

        mock_get.assert_called_once_with(self.get_event, 1908, 2016)

    def test_get_url_params(self):
        self.test_api_function.endpoint = "/test/{test_id}/task/{task_id}"

        params = self.test_api_function._get_url_params()

        self.assertEqual(params, ["test_id", "task_id"])


if __name__ == "__main__":
    unittest.main()
